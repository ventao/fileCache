package com.personal.android.general.video;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;
import com.personal.android.general.R;
import com.personal.android.general.activity.BaseActivity;
import com.personal.android.general.utils.PromptUtils;

/**
 * Created by zhangwentao on 2015/11/3.
 */
public class VideoPlayActivity extends BaseActivity {

    private VideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.video_play_activity);

        mVideoView = (VideoView) findViewById(R.id.video_view);
        String path = getIntent().getStringExtra(VideoListActivity.VIDEO_PATH);
        if (path != null) {
            play(path);
        }
    }

    private void play(String path) {
        mVideoView.setMediaController(new MediaController(this, true));
        mVideoView.setVideoPath(path);
        mVideoView.start();
        mVideoView.requestFocus();
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                PromptUtils.showToast("准备播放");
            }
        });
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                PromptUtils.showToast("抱歉，无法播放");
                return true;
            }
        });
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                PromptUtils.showToast("播放完毕");
                finish();
            }
        });
    }
}
