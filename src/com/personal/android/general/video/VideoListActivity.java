package com.personal.android.general.video;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.personal.android.general.R;
import com.personal.android.general.activity.BaseActivity;
import com.personal.android.general.cache.CacheUtils;
import com.personal.android.general.cache.SerializableObjectKey;

import java.util.List;

/**
 * Created by zhangwentao on 2015/11/3.
 */
public class VideoListActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    private List<String> mVideoPathList;
    private ListView mListView;
    public static final String VIDEO_PATH = "video_path";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.video_list_activity);

        mVideoPathList = (List<String>) CacheUtils.getObjectCache().get(SerializableObjectKey.VIDEO_PATH_LIST);

        mListView = (ListView) findViewById(R.id.video_list);
        mListView.setAdapter(new VideoListAdapter());
        mListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String path = mVideoPathList.get(i);
        if (null != path) {
            startActivity(new Intent(this, VideoPlayActivity.class).putExtra(VIDEO_PATH, path));
        }
    }

    class VideoListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mVideoPathList == null ? 0 : mVideoPathList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = View.inflate(VideoListActivity.this, R.layout.video_list_item, null);
            }
            ((TextView) view.findViewById(R.id.video_path)).setText(mVideoPathList.get(i));
            return view;
        }
    }
}
