package com.personal.android.general.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.personal.android.general.R;

public class MyActivity extends BaseActivity implements View.OnClickListener {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        findViewById(R.id.in_test).setOnClickListener(this);
        findViewById(R.id.timer).setOnClickListener(this);
        findViewById(R.id.time).setOnClickListener(this);

        findViewById(R.id.call).setOnClickListener(this);
        findViewById(R.id.time).setOnClickListener(this);

        findViewById(R.id.expand_list).setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.expand_list:
                startActivity(new Intent(this, ExpandListActivity.class));
                break;
        }
    }
}