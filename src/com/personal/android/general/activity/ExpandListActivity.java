package com.personal.android.general.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import com.personal.android.general.R;
import com.personal.android.general.adapter.ExpandListAdapter;

/**
 * Created by zhangwentao on 2015/12/18.
 */
public class ExpandListActivity extends BaseActivity implements ExpandableListView.OnGroupClickListener {

    private ExpandableListView mExpandableListView;
    private ExpandListAdapter mExpandListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expand_list_activity);
        initView();
    }

    private void initView() {
        mExpandableListView = (ExpandableListView) findViewById(R.id.expand_list);
        mExpandableListView.setGroupIndicator(null);
        mExpandListAdapter = new ExpandListAdapter(this);
        mExpandableListView.setAdapter(mExpandListAdapter);

        for (int i = 0; i < mExpandListAdapter.getGroupCount(); i++) {
            mExpandableListView.expandGroup(i);
        }

        mExpandableListView.setOnGroupClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
        return true;
    }
}
