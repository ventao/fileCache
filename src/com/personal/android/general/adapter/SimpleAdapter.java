package com.personal.android.general.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * Created by winter on 2015/9/23.
 *
 * @author winter
 */
public class SimpleAdapter extends BaseAdapter {

    private Context mContext;

    public SimpleAdapter() {
    }

    public SimpleAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}
