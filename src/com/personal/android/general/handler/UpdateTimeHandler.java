package com.personal.android.general.handler;

import android.os.Message;
import android.widget.Button;
import com.personal.android.general.R;
import com.personal.android.general.activity.MyActivity;

/**
 * Created by zhangwentao on 2015/11/6.
 */
public class UpdateTimeHandler extends SoftReferenceHandler<MyActivity> {

    public final static int UPDATE_TIME_CODE = 0;

    public int mMaxTime = 60;

    public UpdateTimeHandler(MyActivity weakReference) {
        super(weakReference);
    }

    @Override
    protected void handlerMessage(MyActivity softReference, Message msg) {
        switch (msg.what) {
            case UPDATE_TIME_CODE:
                if (mMaxTime > 0) {
                    mMaxTime--;
                    ((Button) softReference.findViewById(R.id.timer)).setText(mMaxTime + "s后重新点击");
                    sendEmptyMessageDelayed(UPDATE_TIME_CODE, 1000);
                } else {
                    softReference.findViewById(R.id.timer).setEnabled(true);
                    ((Button) softReference.findViewById(R.id.timer)).setText("点击进入60s倒计时");
                }
                break;
        }
    }
}
