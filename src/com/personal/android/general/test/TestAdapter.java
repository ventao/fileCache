package com.personal.android.general.test;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.personal.android.general.R;
import com.personal.android.general.adapter.SimpleAdapter;

import java.util.List;

/**
 * Created by winter on 2015/9/23.
 *
 * @author winter
 */
public class TestAdapter extends SimpleAdapter {

    List mList;
    LayoutInflater mLayoutInflater;

    public TestAdapter(Context context) {
        super(context);
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void setList(List list) {
        if (list != mList) {
            mList = list;
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view != null) {
            view = mLayoutInflater.inflate(R.layout.test_list_item, null);
        }
        return view;
    }
}
