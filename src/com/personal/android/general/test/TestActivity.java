package com.personal.android.general.test;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.personal.android.general.R;
import com.personal.android.general.activity.BaseActivity;
import com.personal.android.general.cache.CacheUtils;
import com.personal.android.general.cache.SerializableObjectKey;
import com.personal.android.general.model.BaseResult;
import com.personal.android.general.utils.*;
import com.personal.android.general.video.CameraActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by winter on 2015/9/23.
 *
 * @author winter
 */
public class TestActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);

        findViewById(R.id.save_string).setOnClickListener(this);
        findViewById(R.id.get_string).setOnClickListener(this);
        findViewById(R.id.save_list).setOnClickListener(this);
        findViewById(R.id.get_list).setOnClickListener(this);
        findViewById(R.id.take_video).setOnClickListener(this);
        findViewById(R.id.get_request).setOnClickListener(this);
        mImageView = (ImageView) findViewById(R.id.image);

        ImageUtils.displayImage(ImageUrls.mImages[3], mImageView);

//        testHttpGet();
        testHttpPost();
    }

    private void testFileCache () {
        //测试文件缓存功能
        CacheUtils.getObjectCache().put(SerializableObjectKey.TEST_STRING, "000000");
        String string = (String) CacheUtils.getObjectCache().get(SerializableObjectKey.TEST_STRING);
    }

    private void testHttpGet() {
        String url = "http://www.baidu.com";
        HttpUtil.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String content) {
                //快速解析
                BaseResult baseResult = JsonUtils.mGson.fromJson(content, BaseResult.class);
            }

            @Override
            public void onFailure(Throwable error, String content) {
                super.onFailure(error, content);
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }

    private void testHttpPost() {
        String url = "";
        RequestParams params = new RequestParams();
        params.put("key", "value");
        HttpUtil.post(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String content) {
                super.onSuccess(content);
            }

            @Override
            public void onFailure(Throwable error, String content) {
                super.onFailure(error, content);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_string:
                CacheUtils.getObjectCache().put(SerializableObjectKey.TEST_STRING, "000000");
                break;
            case R.id.get_string:
                String string = (String) CacheUtils.getObjectCache().get(SerializableObjectKey.TEST_STRING);
                PromptUtils.showToast(string);
                break;
            case R.id.save_list:
                //保存对象  类似
                List list = new ArrayList();
                list.add("1");
                list.add("2");
                list.add("3");
                CacheUtils.getObjectCache().put(SerializableObjectKey.TEST_LIST, list);
                break;
            case R.id.get_list:
                List list1 = (List) CacheUtils.getObjectCache().get(SerializableObjectKey.TEST_LIST);
                PromptUtils.showToast((String)list1.get(0) + list1.get(1) + list1.get(2) + "");
                break;

            case R.id.take_video:
                startActivity(new Intent(this, CameraActivity.class));
                break;
            case R.id.get_request:
                testHttpGet();
                break;
        }
    }
}
