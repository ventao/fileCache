package com.personal.android.general.cache;

import android.content.Context;
import android.os.AsyncTask;
import com.personal.android.general.BaseApplication;
import com.personal.android.general.utils.FileUtils;
import com.personal.android.general.utils.LogUtils;
import com.personal.android.general.utils.PromptUtils;

import java.io.File;

/**
 * Created by winter on 2015/9/22.
 *
 * @author winter
 */
public class CacheUtils {
    private static final String OBJECT_PATH = "object";
    private static final float OBJECT_CACHE_SIZE_PERCENT = 0.01f;
    private static final float CURRENT_USER_OBJECT_CACHE_SIZE_PERCENT = 0.02f;

    public static final String IMAGE_PATH = "image";
    private static final float IMAGE_CACHE_SIZE_PERCENT = 0.3f;

    private static SerializableCache mObjectCache;
    private static SerializableCache mCurrentUserObjectCache;

    public static void init() {
        long maxMemory = Runtime.getRuntime().maxMemory();
        LogUtils.i("CacheUtils", "maxMemory" + maxMemory);
        mObjectCache = new SerializableCache((int) (OBJECT_CACHE_SIZE_PERCENT * maxMemory)
                , BaseApplication.getBaseApplication().getBaseCacheDir() + OBJECT_PATH);

        openObjectCache("object");
    }

    public static void openObjectCache(String name) {
        long maxMemory = Runtime.getRuntime().maxMemory();
        mObjectCache = new SerializableCache((int)(CURRENT_USER_OBJECT_CACHE_SIZE_PERCENT * maxMemory)
                , BaseApplication.getBaseApplication().getBaseCacheDir() + OBJECT_PATH + File.separator + name);
    }

    public static SerializableCache getObjectCache() {
        return mObjectCache;
    }

    public static void notifyMemoryLow() {
        mObjectCache.notifyMemoryLow();
    }

    public static void close() {
        mObjectCache.clearMemory();
    }

    public static void clear(final Context context, final Runnable runnable) {
        PromptUtils.showProgressDialog(context, "正在清理缓存", true, true);
        new AsyncTask<Object, Object, Object>() {
            @Override
            protected Object doInBackground(Object... objects) {
                FileUtils.deleteFile(context.getCacheDir());
                FileUtils.deleteFile(context.getFilesDir());
                FileUtils.deleteFile("/data/data/" + context.getPackageName() + "/databases");
                FileUtils.deleteFile("/data/data/" + context.getPackageName() + "/shared_prefs");
                FileUtils.deleteFile(BaseApplication.getBaseApplication().getBaseExternalCacheDir());
                close();
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                init();
                PromptUtils.dismissProgressDialog();
                if (runnable != null) {
                    runnable.run();
                }
            }
        }.execute();
    }
}
