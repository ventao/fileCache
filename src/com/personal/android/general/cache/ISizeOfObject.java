package com.personal.android.general.cache;

/**
 * Created by winter on 2015/9/22.
 *
 * @author winter
 */
public interface ISizeOfObject {
    /**
     * 获取对象内存大小，单位bit
     * @return 内存大小， 单位bit
     */
    int getSize();
}
