package com.personal.android.general.cache;

import android.util.LruCache;
import com.personal.android.general.GeneralUtils;
import com.personal.android.general.thread.StorageThreadPool;
import com.personal.android.general.utils.FileUtils;

import java.io.File;

/**
 * Created by winter on 2015/9/22.
 *
 * @author winter
 */
public class SerializableCache {

    private String mParentPath;
    private LruCache<String, Object> mLruCache;

    public SerializableCache(int maxSize, String parentPath) {
        mLruCache = new LruCache<String, Object>(maxSize) {
            @Override
            protected Object create(String key) {
                return createObject(key);
            }

            @Override
            protected int sizeOf(String key, Object value) {
                int objectSize = 0;
                if (value instanceof ISizeOfObject) {
                    objectSize = ((ISizeOfObject) value).getSize();
                } else {
                    objectSize = GeneralUtils.sizeOf(value);
                }
                return objectSize;
            }
        };

        if (parentPath.endsWith(File.separator)) {
            mParentPath = parentPath;
        } else {
            mParentPath = parentPath + File.separatorChar;
        }
        FileUtils.createPath(parentPath);
    }

    public synchronized Object put(final SerializableObjectKey key, final Object value) {
        StorageThreadPool.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                saveObject(getFilePath(key.getValue()), value);
            }
        });
        return mLruCache.put(key.getValue(), value);
    }

    public synchronized Object get(SerializableObjectKey key) {
        return mLruCache.get(key.getValue());
    }

    public synchronized boolean contain(SerializableObjectKey key) {
        return get(key) != null;
    }

    public synchronized void delete(SerializableObjectKey key) {
        mLruCache.remove(key.getValue());
        FileUtils.deleteFile(getFilePath(key.getValue()));
    }

    public synchronized void clearMemory() {
        try {
            mLruCache.evictAll();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public synchronized void clearMemoryAndDisk() {
        mLruCache.evictAll();
        FileUtils.deleteFile(mParentPath);
        FileUtils.createPath(mParentPath);
    }

    public synchronized void notifyMemoryLow() {
        try {
            mLruCache.trimToSize(mLruCache.size() /2);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private String getFilePath(String key) {
        return mParentPath + FileUtils.translateFileName(key);
    }

    protected Object createObject(String filePath) {
        synchronized (this) {
            return FileUtils.readObject(filePath);
        }
    }

    protected void saveObject(String filePath, Object o) {
        synchronized (this) {
            FileUtils.writeObject(filePath, o);
        }
    }
}
