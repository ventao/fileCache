package com.personal.android.general.cache;

/**
 * Created by winter on 2015/9/22.
 * 序列化对象的key
 * @author winter
 */
public enum  SerializableObjectKey {

    TEST_STRING("test_string"),

    VIDEO_PATH_LIST("video_path_list"),

    TEST_LIST("test_list");

    private String mValue;

    SerializableObjectKey(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }
}
