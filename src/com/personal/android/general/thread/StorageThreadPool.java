package com.personal.android.general.thread;

/**
 * Created by winter on 2015/9/22.
 *
 * @author winter
 */
public class StorageThreadPool extends DefaultThreadPool {

    private static final int MAX_THREAD_COUNT = 1;
    private static final int MAX_QUEUE_COUNT = 20;

    protected StorageThreadPool() {
        super(MAX_THREAD_COUNT, MAX_QUEUE_COUNT);
    }

    private static StorageThreadPool mInstance = new StorageThreadPool();

    public static StorageThreadPool getInstance() {
        return mInstance;
    }
}
